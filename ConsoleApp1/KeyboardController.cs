﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace ConsoleApp1 {
    class KeyboardController : IController {
        private Model model;
        public int getScore() { return model.getScore(); }
        public KeyboardController(Model _model) {
            model = _model;
            Console.WriteLine("Press space to start");
            //model.AfterMoveEvent += makeNextMove;
        }
        public void setTurningMode(TurningModes mode) {
            throw new NotImplementedException();
        }
        public void reloadModel() { throw new NotImplementedException(); }
        public NNetwork.Stats getStats() { throw new NotImplementedException(); }
        public void setStateVectorMode(StateVectorMode mode) { throw new NotImplementedException(); }
        public void makeNextMove() {
            model.turn(getNextMove());
        }
        private Directions getNextMove() {
            ConsoleKeyInfo cki = new ConsoleKeyInfo();
            while (Console.KeyAvailable == false)
                Thread.Sleep(100); // Loop until input is entered.
            cki = Console.ReadKey(true);
            string key = cki.Key.ToString();
            if (key.Equals("UpArrow")) {
                return Directions.UP;
            }
            if (key.Equals("RightArrow")) {
                return Directions.RIGHT;
            }
            if (key.Equals("DownArrow")) {
                return Directions.DOWN;
            }
            if (key.Equals("LeftArrow")) {
                return Directions.LEFT;
            }
            return model.snakeDirection;
        }
        public void play() {
            ConsoleKeyInfo cki = new ConsoleKeyInfo();
            do {
                cki = Console.ReadKey(true);
            } while (!cki.Key.ToString().Equals("Spacebar"));
            Thread modelThread = new Thread(p => {
                ((Model)p).launch();
            });
            modelThread.Start(model);
            do {
                // Your code could perform some useful task in the following loop. However, 
                // for the sake of this example we'll merely pause for a quarter second.
                //while (Console.KeyAvailable == false)
                //    Thread.Sleep(100); // Loop until input is entered.
                makeNextMove();
            } while (model.gameStatus != GameStatus.OVER);
            modelThread.Join();
        }
    }
}
