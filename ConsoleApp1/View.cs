﻿using System;
using System.Threading;

namespace ConsoleApp1 {
    class View {
        private Model model;
        private char wall = (char)1;
        private char snake = (char)35;
        private char food = (char)64;
        private char blank = (char)32;
        private char crashed_head = 'X';
        private string horizontalWallLine = "";
        private bool drawOptimizedFlag = true;
        //private string errors = "";
        //private int speed;
        private int xShift;
        private const string GAME_OVER_LABEL = "GAME IS OVER";
        private const string GAME_WON_LABEL = "GAME HAS BEEN WON";
        public View(Model _model, int _xShift, int _speed = 500) {
            model = _model;
            //speed = _speed;
            xShift = _xShift;
            for (int i = 0; i < model.width + 2; i++) {
                horizontalWallLine += wall;
            }
            model.AfterMoveEvent += redraw;
            draw(drawBorders: true);
        }
        private void redraw() {
            draw(drawBorders: false);
        }
        private void draw(bool drawBorders) {
            if (drawOptimizedFlag && !drawBorders) {
                drawOptimized(drawBorders: drawBorders);
            }
            else {
                drawComplete(drawBorders: drawBorders);
            }
        }
        private void drawComplete(bool drawBorders) {
            if (drawBorders) {
                Console.SetCursorPosition(0, xShift);
                for (int j = 0; j < model.width + 2; ++j) {
                    System.Console.Write(wall);
                }
            }
            for (int i = 0; i < model.height; ++i) {
                string line = "";
                if (drawBorders) {
                    line += wall;
                }
                for (int j = 0; j < model.width; ++j) {
                    Point currentPoint = new Point(i, j);
                    if (model.gameStatus == GameStatus.OVER && model.crashedLocation != null && model.crashedLocation.Equals(currentPoint)) {
                        line += crashed_head;
                    }
                    else if (model.isIn(currentPoint)) {
                    //else if (model.isIn(i, j)) {
                        line += snake;
                    }
                    else if (model.food.Equals(currentPoint)) {
                    //else if (model.food.Equals(i, j)) {
                        line += food;
                    }
                    else {
                        line += blank;
                    }
                }
                if (drawBorders) {
                    line += wall;
                    Console.SetCursorPosition(0, xShift + i + 1);
                }
                else {
                    Console.SetCursorPosition(1, xShift + i + 1);
                }
                Console.WriteLine(line);
                if (model.gameStatus == GameStatus.OVER && model.crashedLocation != null) {
                    Console.SetCursorPosition(model.crashedLocation.y + 1, xShift + model.crashedLocation.x + 1);
                    Console.Write(crashed_head);
                }
            }
            if (drawBorders) {
                for (int j = 0; j < model.width + 2; ++j) {
                    System.Console.Write(wall);
                }
                System.Console.WriteLine();
            }
            Console.SetCursorPosition(0, xShift + model.height + 3);
            for (int i = 0; i < 5; ++i) {
                Console.WriteLine("                                                            ");
            }
            Console.SetCursorPosition(0, xShift + model.height + 3);
            Console.WriteLine(model.getErrors());
            if (model.gameStatus == GameStatus.OVER) {
                System.Console.WriteLine(View.GAME_OVER_LABEL);
                System.Console.WriteLine("Your score:" + model.getScore());
            }
        }
        private void cleanUpTail(Point p) {
            if (!model.isWall(p) && !model.isIn(p)) {
                Console.SetCursorPosition(p.y + 1, xShift + p.x + 1);
                Console.Write(blank);
            }
        }
        private void drawOptimized(bool drawBorders) {
            if (drawBorders) {
                Console.SetCursorPosition(0, xShift);
                System.Console.Write(horizontalWallLine);
                for (int i = 0; i < model.height; ++i) {
                    Console.SetCursorPosition(0, xShift + i + 1);
                    Console.Write(wall);
                    Console.SetCursorPosition(model.width + 1, xShift + i + 1);
                    Console.Write(wall);
                }
                Console.SetCursorPosition(0, xShift + model.height + 1);
                System.Console.Write(horizontalWallLine);
            }
            var p = model.snake.First.Value;
            Console.SetCursorPosition(p.y + 1, xShift + p.x + 1);
            Console.Write(snake);
            p = new Point(model.snake.Last.Value.x, model.snake.Last.Value.y);
            p.x += 1;
            cleanUpTail(p);
            p.x -= 2;
            cleanUpTail(p);
            p.x += 1;
            p.y += 1;
            cleanUpTail(p);
            p.y -= 2;
            cleanUpTail(p);
            p = model.food;
            Console.SetCursorPosition(p.y + 1, xShift + p.x + 1);
            Console.Write(food);
            if (model.gameStatus == GameStatus.OVER && model.crashedLocation != null ) {
                p = model.crashedLocation;
                Console.SetCursorPosition(p.y + 1, xShift + p.x + 1);
                Console.Write(crashed_head);
            }
            //Console.SetCursorPosition(0, xShift + model.height + 3);
            //for (int i = 0; i < 5; ++i) {
            //    Console.WriteLine("                                                            ");
            //}
            //if (!errors.Equals("")) {
            //    Console.SetCursorPosition(0, xShift + model.height + 3);
            //    Console.WriteLine("                                                            ");
            //    Console.WriteLine("                                                            ");
            //    Console.WriteLine("                                                            ");
            //    Console.WriteLine("                                                            ");
            //    Console.WriteLine("                                                            ");
            //}
            //errors = model.getErrors();
            //if (!errors.Equals("")) {
            //    Console.SetCursorPosition(0, xShift + model.height + 3);
            //    Console.WriteLine(errors);
            //}
            if (model.gameStatus == GameStatus.OVER) {
                Console.SetCursorPosition(0, xShift + model.height + 3);
                Console.SetCursorPosition(0, xShift + model.height + 3);
                Console.WriteLine(View.GAME_OVER_LABEL);
                Console.WriteLine("Your score:" + model.getScore());
            }
            if (model.gameStatus == GameStatus.WON) {
                Console.SetCursorPosition(0, xShift + model.height + 3);
                Console.SetCursorPosition(0, xShift + model.height + 3);
                Console.WriteLine(View.GAME_WON_LABEL);
                Console.WriteLine("Your score:" + model.getScore());
            }
        }
        public void displayGame() {
            draw(drawBorders:true);
            //while (model.gameStatus != GameStatus.OVER) {
            //    Thread.Sleep(speed);
            //    draw();
            //}
        }
    }
}
