﻿namespace ConsoleApp1 {
    public enum TurningModes {
        ABSOLUTE,
        RELATIVE
    }
    public enum StateVectorMode {
        MODE1,
        MODE2,
        MODE3,
        MODE4
    }
    interface IController {
        void makeNextMove();
        void play();
        int getScore();
        void reloadModel();
        NNetwork.Stats getStats();
        void setTurningMode(TurningModes mode);
        void setStateVectorMode(StateVectorMode mode);
    }
}