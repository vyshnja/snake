﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace ConsoleApp1 {
    using NNetwork;
    class AutoNNControllerV2 : IController {
        private Model model;
        private double maxDistance;
        private int STEPS_PER_FOOD = 150;
        private int stepsLeft;
        private TurningModes turningMode;
        public static Stats stats;
        public NNetwork1 nnetwork { get; private set; }
        public void setStateVectorMode(StateVectorMode mode) { throw new NotImplementedException(); }
        public void setTurningMode(TurningModes mode) {
            turningMode = mode;
        }
        Thread modelThread;
        /// <summary>
        /// Food count consumed by a snake in current game
        /// </summary>
        public int score { get; private set; }
        public int getScore() { return score; }
        public Stats getStats() { return stats; }
        /// <summary>
        /// Initialize controller with randomly generated game board
        /// and specific neural network
        /// </summary>
        public AutoNNControllerV2(NNetwork1 _nnetwork) : this(new Model(20, 20, 0), _nnetwork) { }
        /// <summary>
        /// Initialize controller with specific game board
        /// and specific neural network
        /// </summary>
        public AutoNNControllerV2(Model _model, NNetwork1 _nnetwork) {
            reloadModel(_model);
            nnetwork = _nnetwork;
            if(stats == null) {
                stats = new Stats(nnetwork.inputCount, nnetwork.outputCount);
            }
        }
        /// <summary>
        /// Generate a new random board for this controller
        /// </summary>
        public void reloadModel() {
            Model _model = new Model(Config.DEFAULT_HEIGHT, Config.DEFAULT_WIDTH, _speed: 0);
            reloadModel(_model);
        }
        /// <summary>
        /// Change game board for this controller to the specified one
        /// </summary>
        /// <param name="_model"></param>
        public void reloadModel(Model _model) {
            model = _model;
            stepsLeft = STEPS_PER_FOOD;
            maxDistance = Math.Sqrt((model.height - 1) * (model.height - 1) + (model.width - 1) * (model.width - 1));
            model.AfterMoveEvent += makeNextMove;
            model.AfterGrowthEvent += snakeGrew;
        }
        private double getFoodNeuron(Point requestedDirectionVector) {
            return getFoodNeuron(requestedDirectionVector, model.head());
        }
        private double getFoodNeuron(Point requestedDirectionVector, Point _head) {
            //return _head.plus(requestedDirectionVector).distance(model.food) / maxDistance;
            double currentFoodDistance = _head.distance(model.food);
            double newFoodDistance = _head.plus(requestedDirectionVector).distance(model.food);
            return newFoodDistance / maxDistance;
            //if(newFoodDistance < currentFoodDistance)
            //    return newFoodDistance / maxDistance;
            //else
            //    return -newFoodDistance / maxDistance;
        }
        private double getWallNeuron(Point requestedDirectionVector) {
            double currentWallDistance = getWallDistance(model.head());
            double newWallDistance = getWallDistance(model.head().plus(requestedDirectionVector));
            return newWallDistance / maxDistance;
            //if (currentWallDistance < newWallDistance)
            //    return newWallDistance / maxDistance;
            //else
            //    return -newWallDistance / maxDistance;
        }
        private double getWallDistance(Point _head) {
            int dx = Math.Min(_head.x, model.width), dy = Math.Min(_head.y, model.height);
            double wallDistance = Math.Sqrt((double)dx * dx + dy * dy);
            return wallDistance;
        }
        private double getSnakeNeuron(Point requestedDirectionVector) {
            return getSnakeNeuron(requestedDirectionVector, model.head());
        }
        private double getSnakeNeuron(Point requestedDirectionVector, Point _head) {
            Point pointAhead = _head.plus(requestedDirectionVector);
            int stepsCounter = 0;
            while (!model.hasCrashed(pointAhead)) {
                pointAhead = pointAhead.plus(requestedDirectionVector);
                stepsCounter++;
            }
            return pointAhead.distance(_head) / maxDistance;
        }
        private Vector getDirectionStateVector(Point requestedDirectionVector) {
            Vector res = new Vector(3);
            res.vs[0] = getFoodNeuron(requestedDirectionVector);
            res.vs[1] = getWallNeuron(requestedDirectionVector);
            res.vs[2] = getSnakeNeuron(requestedDirectionVector);
            return res;
        }
        private Vector getModelStateVector() {
            Vector res = new Vector(nnetwork.inputCount);
            int offset = 0;
            res.merge(getDirectionStateVector(new Point(-1, -1)), offset);
            offset += 3;
            res.merge(getDirectionStateVector(new Point(-1, 0)), offset);
            offset += 3;
            res.merge(getDirectionStateVector(new Point(-1, 1)), offset);
            offset += 3;
            res.merge(getDirectionStateVector(new Point(0, 1)), offset);
            offset += 3;
            res.merge(getDirectionStateVector(new Point(1, 1)), offset);
            offset += 3;
            res.merge(getDirectionStateVector(new Point(1, 0)), offset);
            offset += 3;
            res.merge(getDirectionStateVector(new Point(1, -1)), offset);
            offset += 3;
            res.merge(getDirectionStateVector(new Point(0, -1)), offset);
            return res;
        }
        public void makeNextMove() {
            if (stepsLeft-- > 0) {
                Vector modelState = getModelStateVector();
                Vector resultingVector = nnetwork.run(modelState);
                int firedNeuron;
                switch (turningMode) {
                    case TurningModes.ABSOLUTE:
                        Directions dir;
                        int counter = 0;
                        Vector copy = new Vector(resultingVector);
                        //find valid direction with the highest neuron output
                        do {
                            firedNeuron = copy.maxIndex();
                            dir = (Directions)firedNeuron;
                            copy.vs[firedNeuron] = Double.MinValue;
                            counter++;
                        }
                        //check if we need to pick the next highest if this move is invalid
                        while (!model.turn(dir) && counter < resultingVector.vs.Length - 1);
                        break;
                    case TurningModes.RELATIVE:
                        firedNeuron = resultingVector.maxIndex();
                        switch (firedNeuron) {
                            case 0: model.turnLeft(); break;
                            case 1: model.turnRight(); break;
                            default: break;
                        }
                        break;
                    default:
                        throw new InvalidOperationException("Invalid turning mode:" + turningMode);
                }
                stats.resultingNeuronIndex[firedNeuron]++;
                stats.stepsTaken++;
                stats.totalSnakeState.add(modelState);
                stats.totalResultingVector.add(resultingVector);
            }
            else {
                model.gameStatus = GameStatus.OVER;
                stats.ranOutOfStepsCounter++;
            }
        }
        private void snakeGrew(){
            stepsLeft += STEPS_PER_FOOD;
        }
        public void play() {
            modelThread = new Thread(p => {
                ((Model)p).launch();
            });
            modelThread.Start(model);
            modelThread.Join();
            score = model.getScore();
        }
    }
}
