﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace ConsoleApp1 {
    class AutoSpiralController : IController {
        private Model model;
        private int moveCounter = 0;
        private int mod = 2;
        public int getScore() { return model.getScore(); }
        public void reloadModel() { throw new NotImplementedException(); }
        public void setStateVectorMode(StateVectorMode mode) { throw new NotImplementedException(); }
        public void setTurningMode(TurningModes mode) {
            throw new NotImplementedException();
        }
        public NNetwork.Stats getStats() { throw new NotImplementedException(); }
        public AutoSpiralController(Model _model) {
            model = _model;
            model.AfterMoveEvent += makeNextMove;
        }
        public void makeNextMove() {
            if(++moveCounter % mod == 0) {
                model.turnRight();
                mod = (mod + 1) % 10;
                if (mod == 0)
                    mod = 2;
            }
        }

        public void play() {
            Thread modelThread = new Thread(p => {
                ((Model)p).launch();
            });
            modelThread.Start(model);
            modelThread.Join();
        }
    }
}
