﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace ConsoleApp1 {
    class ConsoleCopy : IDisposable {

        FileStream fileStream;
        StreamWriter fileWriter;
        TextWriter doubleWriter;
        TextWriter oldOut;
        public static string path_base;

        class DoubleWriter : TextWriter {

            TextWriter one;
            TextWriter two;

            public DoubleWriter(TextWriter one, TextWriter two) {
                this.one = one;
                this.two = two;
            }

            public override Encoding Encoding {
                get { return one.Encoding; }
            }

            public override void Flush() {
                one.Flush();
                two.Flush();
            }

            public override void Write(char value) {
                one.Write(value);
                two.Write(value);
            }

        }

        public ConsoleCopy() {
            oldOut = Console.Out;
            //path_base = DateTime.Now.GetDateTimeFormats('s')[0];
            path_base = DateTime.Now.GetDateTimeFormats('s')[0].Replace('-', '_').Replace('T', '_').Replace(':', '_');
            string path = path_base + ".log";
            Console.WriteLine("Log file: " + path);
            
            try {
                fileStream = File.Create(path);

                fileWriter = new StreamWriter(fileStream);
                fileWriter.AutoFlush = true;

                doubleWriter = new DoubleWriter(fileWriter, oldOut);
            }
            catch (Exception e) {
                Console.WriteLine("Cannot open file for writing");
                Console.WriteLine(e.Message);
                return;
            }
            Console.SetOut(doubleWriter);
        }

        public void Dispose() {
            Console.SetOut(oldOut);
            if (fileWriter != null) {
                fileWriter.Flush();
                fileWriter.Close();
                fileWriter = null;
            }
            if (fileStream != null) {
                fileStream.Close();
                fileStream = null;
            }
        }

    }
}
