﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace ConsoleApp1 {
    using NNetwork;
    class AutoNNController : IController {
        private Model model;
        private double maxDistance;
        private int STEPS_PER_FOOD = 500;
        private int stepsLeft;
        private TurningModes turningMode;
        public static Stats stats;
        public NNetwork1 nnetwork { get; private set; }
        public void setStateVectorMode(StateVectorMode mode) { throw new NotImplementedException(); }
        public void setTurningMode(TurningModes mode) {
            turningMode = mode;
        }
        Thread modelThread;
        /// <summary>
        /// Food count consumed by a snake in current game
        /// </summary>
        public int score { get; private set; }
        public int getScore() { return score; }
        public Stats getStats() { return stats; }
        /// <summary>
        /// Initialize controller with randomly generated game board
        /// and randomly generated neural network
        /// </summary>
        //public AutoNNController(): this(new Model(20, 20, 0), new NNetwork1()) {}
        /// <summary>
        /// Initialize controller with specific game board
        /// and randomly generated neural network
        /// </summary>
        public AutoNNController(Model _model) :this(_model, new NNetwork1()) {}
        /// <summary>
        /// Initialize controller with randomly generated game board
        /// and specific neural network
        /// </summary>
        public AutoNNController(NNetwork1 _nnetwork) :this(new Model(20, 20, 0), _nnetwork) {}
        /// <summary>
        /// Initialize controller with specific game board
        /// and specific neural network
        /// </summary>
        public AutoNNController(Model _model, NNetwork1 _nnetwork) {
            reloadModel(_model);
            nnetwork = _nnetwork;
            if (stats == null) {
                stats = new Stats(nnetwork.inputCount, nnetwork.outputCount);
            }
        }
        /// <summary>
        /// Generate a new random board for this controller
        /// </summary>
        public void reloadModel() {
            Model _model = new Model(Config.DEFAULT_HEIGHT, Config.DEFAULT_WIDTH, _speed: 0);
            reloadModel(_model);
        }
        public void reoladNetwork(NNetwork1 _nnetwork) {
            this.nnetwork = _nnetwork;
        }
        /// <summary>
        /// Change game board for this controller to the specified one
        /// </summary>
        /// <param name="_model"></param>
        public void reloadModel(Model _model) {
            model = _model;
            stepsLeft = STEPS_PER_FOOD;
            maxDistance = Math.Sqrt((model.height - 1) * (model.height - 1) + (model.width - 1) * (model.width - 1));
            model.AfterMoveEvent += makeNextMove;
            model.AfterGrowthEvent += snakeGrew;
        }
        private int getFoodNeuron(Point requestedDirectionVector) {
            return getFoodNeuron(requestedDirectionVector, model.head());
        }
        private int getFoodNeuron(Point requestedDirectionVector, Point _head) {
            Point foodDirectionVector = new Point(model.food.x - _head.x,
                                    model.food.y - _head.y);
            if ((foodDirectionVector.x * requestedDirectionVector.y == foodDirectionVector.y * requestedDirectionVector.x) &&
                Math.Sign(foodDirectionVector.x) == Math.Sign(requestedDirectionVector.x) &&
                Math.Sign(foodDirectionVector.y) == Math.Sign(requestedDirectionVector.y)){
                return 1;
            }
            else {
                return 0;
            }
        }
        private double getWallNeuron(Point requestedDirectionVector) {
            return getWallNeuron(requestedDirectionVector, model.head());
        }
        private double getWallNeuron(Point requestedDirectionVector, Point _head) {
            int dx = 0, dy = 0;
            double wallDistance;
            if (requestedDirectionVector.x > 0) {
                dx = model.height - _head.x;
            }
            else if (requestedDirectionVector.x < 0) {
                dx = _head.x;
            }
            if (requestedDirectionVector.y > 0) {
                dy = model.width - _head.y;
            }
            else if (requestedDirectionVector.y < 0) {
                dy = _head.y;
            }
            if (requestedDirectionVector.x != 0 && requestedDirectionVector.y != 0) {
                dx = dy = Math.Min(dx, dy);
            }
            wallDistance = Math.Sqrt((double)dx * dx + dy * dy) / maxDistance;
            return wallDistance;
        }
        private double getSnakeNeuron(Point requestedDirectionVector) {
            return getSnakeNeuron(requestedDirectionVector, model.head());
        }
        private double getSnakeNeuron(Point requestedDirectionVector, Point _head) {
            Point pointAhead = _head.plus(requestedDirectionVector);
            int stepsCounter = 0;
            while (!model.hasCrashed(pointAhead)) {
                pointAhead = pointAhead.plus(requestedDirectionVector);
                stepsCounter++;
            }
            return pointAhead.distance(_head) / maxDistance;
        }
        private Vector getDirectionStateVector(Point requestedDirectionVector) {
            Vector res = new Vector(3);
            res.vs[0] = getFoodNeuron(requestedDirectionVector);
            res.vs[1] = getWallNeuron(requestedDirectionVector);
            res.vs[2] = getSnakeNeuron(requestedDirectionVector);
            return res;
        }
        private Vector getModelStateVector() {
            Vector res = new Vector(nnetwork.inputCount);
            int offset = 0;
            res.merge(getDirectionStateVector(new Point(-1, -1)), offset);
            offset += 3;
            res.merge(getDirectionStateVector(new Point(-1, 0)), offset);
            offset += 3;
            res.merge(getDirectionStateVector(new Point(-1, 1)), offset);
            offset += 3;
            res.merge(getDirectionStateVector(new Point(0, 1)), offset);
            offset += 3;
            res.merge(getDirectionStateVector(new Point(1, 1)), offset);
            offset += 3;
            res.merge(getDirectionStateVector(new Point(1, 0)), offset);
            offset += 3;
            res.merge(getDirectionStateVector(new Point(1, -1)), offset);
            offset += 3;
            res.merge(getDirectionStateVector(new Point(0, -1)), offset);
            return res;
        }
        public void makeNextMove() {
            if (stepsLeft-- > 0) {
                Vector modelState = getModelStateVector();
                Vector resultingVector = nnetwork.run(modelState);
                int firedNeuron = resultingVector.maxIndex();
                stats.resultingNeuronIndex[firedNeuron]++;
                Directions dir = (Directions)firedNeuron;
                model.turn(dir);
                stats.stepsTaken++;
                stats.totalSnakeState.add(modelState);
                stats.totalResultingVector.add(resultingVector);
            }
            else {
                Console.WriteLine("Ran out of steps");
                modelThread.Abort();
            }
        }
        private void snakeGrew(){
            stepsLeft += STEPS_PER_FOOD;
        }
        public void play() {
            modelThread = new Thread(p => {
                ((Model)p).launch();
            });
            modelThread.Start(model);
            modelThread.Join();
            score = model.getScore();
        }
        public void testFoodNeuronAll() {
            testFoodNeuron(new Point(0, 1));
            testFoodNeuron(new Point(-1, 0));
            testFoodNeuron(new Point(-1, 1));
            testFoodNeuron(new Point(-1, -1));
        }
        public void testFoodNeuron(Point direction) {
            Console.WriteLine("Head:" + model.head() + "; Food:" + model.food);
            int counter = 0;
            for (int i = 0; i < model.height; ++i) {
                Console.Write(" ");
                for (int j = 0; j < model.width; ++j) {
                    counter += getFoodNeuron(direction, new Point(i, j));
                    Console.Write(getFoodNeuron(direction, new Point(i, j)));
                }
                Console.WriteLine();
            }
            Console.WriteLine("# of Hits:" + counter);
        }
        public void testWallNeuron(Point direction) {
            Console.WriteLine("Head:" + model.head());
            int counter = 0;
            for (int i = 0; i < model.height; ++i) {
                Console.Write(" ");
                for (int j = 0; j < model.width; ++j) {
                    Console.Write(Math.Round(getWallNeuron(direction, new Point(i, j)), 2) + ";");
                }
                Console.WriteLine();
            }
            Console.WriteLine("# of Hits:" + counter);
        }
    }
}
