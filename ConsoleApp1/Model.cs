﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace ConsoleApp1 {
    internal class Point {
        public int x, y;
        public Point(int _x, int _y) {
            x = _x;
            y = _y;
        }
        public Point plus(Point p1) {
            return new Point(this.x + p1.x, this.y + p1.y);
        }
        public Point minus(Point p1) {
            return new Point(this.x - p1.x, this.y - p1.y);
        }
        public double distance(Point p1) {
            return Math.Sqrt((p1.x - this.x) * (p1.x - this.x) + (p1.y - this.y) * (p1.y - this.y));
        }
        //hash code is zig-zag traverse of matrix similar to the proof
        //in countability theorem
        public override int GetHashCode() {
            // total number of elements in the "triangle" up to the current diagonal
            int res = (1 + x + y) * (x + y) / 2;
            //add position within current diagonal
            res += x;
            return res;
        }
        public override bool Equals(object obj) {
            Point p = (Point)obj;
            return p.x == this.x && p.y == this.y;
        }
        public bool Equals(int x, int y) {
            return x == this.x && y == this.y;
        }
        public override string ToString() {
            return x + ";" + y;
        }
    }
    public enum Directions {
        UP,
        RIGHT,
        DOWN,
        LEFT
    };
    public enum GameStatus {
        READY,
        PLAYING,
        OVER,
        WON
    }
    class Model {
        private Queue<string> errors;
        private Point[] diffs = {new Point(-1, 0), new Point(0, 1), new Point(1, 0), new Point(0, -1)};
        //private Point[] diffs = {new Point(0, -1), new Point(1, 0), new Point(0, 1), new Point(-1, 0)};
        private Random rand = new Random();
        private bool turnedOnThisMove = false;
        private int speed;
        public Point crashedLocation;
        public readonly int width, height;
        private readonly int boardSize;
        public LinkedList<Point> snake;
        private int initSize;
        //public LinkedList<Point> freeSpace;
        public Point food;
        public GameStatus gameStatus = GameStatus.READY;
        private bool[,] board;
        // declare delegate 
        public delegate void AfterMove();
        public delegate void AfterGrowth();
        public Directions snakeDirection { get; private set; }
        private int mod(int x, int m) {
            return (x % m + m) % m;
        }
        public void turnRight() {
            this.turn((Directions)mod((int)this.snakeDirection + 1, 4));
            //this.turn((Directions)Math.Abs((((int)this.snakeDirection + 1) % 4)));
        }
        public void turnLeft() {
            this.turn((Directions)mod((int)this.snakeDirection - 1, 4));
        }

        //declare event of type delegate
        public event AfterMove AfterMoveEvent;
        public event AfterGrowth AfterGrowthEvent;
        public Model(int _height, int _width, int _speed, bool initLarge = false) {
            this.width = _width;
            this.height = _height;
            this.boardSize = width * height;
            this.speed = _speed;
            snakeDirection = Directions.UP;
            snake = new LinkedList<Point>();
            board = new bool[height, width];
            for(int i = 0; i < height; ++i) {
                for (int j = 0; j < width; ++j) {
                    board[i, j] = false;
                }
            }
            //freeSpace = new LinkedList<Point>();
            //for (int i = 0; i < height; ++i) {
            //    for (int j = 0; j < width; ++j) {
            //        freeSpace.AddLast(new Point(i, j));
            //    }
            //}
            if (!initLarge) {
                Point head = new Point(height / 2, width / 2);
                snake.AddFirst(head);
                board[head.x, head.y] = true;
            }
            else {
                Point dir = new Point(1, 0);
                Point tail = new Point(height / 4, 0);
                int initSize = 3 * height * width / 8 + height + width;
                for (; snake.Count < initSize && tail.x < height - 1;) {
                    tail = tail.plus(dir);
                    snake.AddLast(tail);
                    board[tail.x, tail.y] = true;
                }
                dir = new Point(0, 1);
                for (; snake.Count < initSize && tail.y < width - 1;) {
                    tail = tail.plus(dir);
                    snake.AddLast(tail);
                    board[tail.x, tail.y] = true;
                }
                dir = new Point(-1, 0);
                for (; snake.Count < initSize && tail.x > 0;) {
                    tail = tail.plus(dir);
                    snake.AddLast(tail);
                    board[tail.x, tail.y] = true;
                }
                for (; snake.Count < initSize && tail.y > width / 2;) {
                    tail = tail.plus(new Point(0, -1));
                    snake.AddLast(tail);
                    board[tail.x, tail.y] = true;
                    dir = new Point(1, 0);
                    for (; snake.Count < initSize && tail.x < 3 * height / 4;) {
                        tail = tail.plus(dir);
                        snake.AddLast(tail);
                        board[tail.x, tail.y] = true;
                    }
                    tail = tail.plus(new Point(0, -1));
                    snake.AddLast(tail);
                    dir = new Point(-1, 0);
                    for (; snake.Count < initSize && tail.x > 0;) {
                        tail = tail.plus(dir);
                        snake.AddLast(tail);
                        board[tail.x, tail.y] = true;
                    }
                }
            }
            //freeSpace.Remove(head);
            initSize = snake.Count;
            generateFood();
            errors = new Queue<string>();
        }
        public Point head() {
            return snake.First.Value;
        }
        public int getScore() {
            return snake.Count - initSize;
        }
        public bool turn(Directions newDirection) {
            bool res = true;
            if (!turnedOnThisMove) {
                int delta = Math.Abs((newDirection - snakeDirection) % 4);
                if (delta != 2) {
                    snakeDirection = newDirection;
                    res = true;
                    turnedOnThisMove = true;
                }
                else {
                    res = false;
                    errors.Enqueue("Invalid turn:" + newDirection.ToString());
                }
            }
            else {
                res = false;
                errors.Enqueue("Already turned on this move. Can't turn again:" + newDirection.ToString());
            }
            return res;
        }
        public bool isIn(Point p) {
            //bool res = false;
            //for (var i = snake.First; i != null; i = i.Next) {
            //    if (i.Value.x == p.x && i.Value.y == p.y) {
            //        res = true;
            //        break;
            //    }
            //}
            //return res;
            return board[p.x, p.y];
        }
        public bool isIn(int x, int y) {
            //bool res = false;
            //for (var i = snake.First; i != null; i = i.Next) {
            //    if (i.Value.x == x && i.Value.y == y) {
            //        res = true;
            //        break;
            //    }
            //}
            //return res;
            return board[x, y];
        }
        public bool isWall(Point p) {
            bool res = false;
            if (p.x < 0 || p.y < 0 || p.x >= height || p.y >= width) {
                res = true;
            }
            return res;
        }
        public bool hasCrashed(Point head) {
            bool res = false;
            if (isWall(head) || isIn(head)) {
                res = true;
            }
            return res;
        }
        private void generateFood() {
            //var freeSpotIndex = rand.Next(0, freeSpace.Count);
            //var i = freeSpace.First;
            //for (int counter = 0; i != null && counter < freeSpotIndex; i = i.Next, counter++);
            //if (i == null)
            //    throw new IndexOutOfRangeException("Could not find a free tile to place food");
            //food = i.Value;
            //freeSpace.Remove(i);
            //var freeSpotIndex = rand.Next(0, boardSize - snake.Count);
            //int counter = 0;
            //for (int i = 0; i < height; ++i) {
            //    for (int j = 0; j < width; ++j) {
            //        //if(!isIn(new Point(i, j))){
            //        if(!isIn(i, j)){
            //            if (counter == freeSpotIndex) {
            //                food = new Point(i, j);
            //                return;
            //            }
            //            counter++;
            //        }
            //    }
            //}
            //throw new IndexOutOfRangeException("Could not find a free tile to place food");
            do {
                food = new Point(rand.Next(0, height), rand.Next(0, width));
            } while (isIn(food));
        }
        public void move() {
            if (gameStatus == GameStatus.PLAYING) {
                Point head = snake.First.Value.plus(diffs[(int)snakeDirection]);
                if (!hasCrashed(head)) {
                    snake.AddFirst(head);
                    board[head.x, head.y] = true;
                    //freeSpace.Remove(head);
                    if (!head.Equals(food)) {
                        //freeSpace.AddLast(snake.Last.Value);
                        board[snake.Last.Value.x, snake.Last.Value.y] = false;
                        snake.RemoveLast();
                    }
                    else {
                        if (snake.Count == boardSize) {
                            gameStatus = GameStatus.WON;
                        }
                        else {
                            generateFood();
                            if (AfterGrowthEvent != null) {
                                AfterGrowthEvent();
                            }
                        }
                    }
                }
                else {
                    gameStatus = GameStatus.OVER;
                    this.crashedLocation = head;
                }
                turnedOnThisMove = false;
                if (AfterMoveEvent != null) {
                    AfterMoveEvent();
                }
            }
        }
        public string getErrors() {
            string res = "";
            while(errors.Count > 0) {
                res += errors.Dequeue() + "\n";
            }
            return res;
        }
        public void launch() {
            gameStatus = GameStatus.PLAYING;
            while (gameStatus != GameStatus.OVER && gameStatus != GameStatus.WON) {
                Thread.Sleep(speed);
                move();
            }
        }
    }
}
