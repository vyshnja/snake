﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1 {
    public static class Config {
        public static double MUTATION_RATE = 0.1;
        public static double RANDOM_ADDITION_TO_EACH_GENERATION = 1.1;
        public static int TRIALS_PER_NETWORK = 1;
        public static int NETWORKS_PER_GENERATION = 5000;
        public static int GENERATIONS = 200;
        public static int MIN_NUMBER_TO_REPRODUCE = 20;
        public static int TRIAL_OUTCOME_SIZE = 100;
        public static double CUTOFF_TO_REPRODUCE = 1;
        public static int STEPS_PER_FOOD = 300;
        //
        public static int DEFAULT_HEIGHT = 20;
        public static int DEFAULT_WIDTH = 20;
        public static int DEFAULT_SPEED = 0;
        public static bool INIT_LARGE = false;
        //
        public static int INPUT_COUNT;
        public static int DEFAULT_LAYER2_COUNT = 20;
        public static int DEFAULT_LAYER3_COUNT = 20;
        public static int OUTPUT_COUNT;
        private static int[] STATE_VECTOR_SIZE = { 28, 28, 24, 24};
        private static int[] OUTPUT_VECTOR_SIZE = { 4, 3 };
        public static StateVectorMode STATE_VECTOR = StateVectorMode.MODE4;
        public static TurningModes TURNING_METHOD = TurningModes.ABSOLUTE;
        public static NNetwork.CrossoverMode CROSSOVER_METHOD = NNetwork.CrossoverMode.OUTCOMING_IN_BLOCKS;
        public static NNetwork.ReproductionRandomnessMethod REPRODUCTION_RANDOMNESS = NNetwork.ReproductionRandomnessMethod.SKEWED_TO_BEGINNING;
        static Config() {
            INPUT_COUNT = Config.STATE_VECTOR_SIZE[(int)STATE_VECTOR];
            OUTPUT_COUNT = Config.OUTPUT_VECTOR_SIZE[(int)TURNING_METHOD];
        }
        public static string toString() {
            string res = "";
            res += "MUTATION_RATE:                      " + MUTATION_RATE + "\r\n" +
                    "RANDOM_ADDITION_TO_EACH_GENERATION: " + RANDOM_ADDITION_TO_EACH_GENERATION + "\r\n" +
                    "TRIALS_PER_NETWORK:                 " + TRIALS_PER_NETWORK + "\r\n" +
                    "NETWORKS_PER_GENERATION:            " + NETWORKS_PER_GENERATION + "\r\n" +
                    "GENERATIONS:                        " + GENERATIONS + "\r\n" +
                    "MIN_NUMBER_TO_REPRODUCE:            " + MIN_NUMBER_TO_REPRODUCE + "\r\n" +
                    "TRIAL_OUTCOME_SIZE:                 " + TRIAL_OUTCOME_SIZE + "\r\n" +
                    "CUTOFF_TO_REPRODUCE:                " + CUTOFF_TO_REPRODUCE + "\r\n" +
                    "STEPS_PER_FOOD:                     " + STEPS_PER_FOOD + "\r\n" +
                    "DEFAULT_HEIGHT:                     " + DEFAULT_HEIGHT + "\r\n" +
                    "DEFAULT_WIDTH:                      " + DEFAULT_WIDTH +"\r\n" +
                    "DEFAULT_SPEED:                      " + DEFAULT_SPEED +"\r\n" +
                    "INIT_LARGE:                         " + INIT_LARGE +"\r\n" +
                    "INPUT_COUNT:                        " + INPUT_COUNT + "\r\n" +
                    "DEFAULT_LAYER2_COUNT:               " + DEFAULT_LAYER2_COUNT + "\r\n" +
                    "DEFAULT_LAYER3_COUNT:               " + DEFAULT_LAYER3_COUNT + "\r\n" +
                    "OUTPUT_COUNT:                       " + OUTPUT_COUNT + "\r\n" +
                    "STATE_VECTOR:                       " + STATE_VECTOR + "\r\n" +
                    "TURNING_METHOD:                     " + TURNING_METHOD + "\r\n" +
                    "CROSSOVER_METHOD:                   " + CROSSOVER_METHOD + "\r\n" +
                    "REPRODUCTION_RANDOMNESS:            " + REPRODUCTION_RANDOMNESS;
            return res;
        }
    }
}
