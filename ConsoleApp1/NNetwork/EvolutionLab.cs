﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace ConsoleApp1.NNetwork {
    public enum ReproductionRandomnessMethod {
        EVENLY_DISTRIBUTES,
        SKEWED_TO_BEGINNING
    }
    class EvolutionLab {
        static private Random rand = new Random();
        private int experimentCounter = 1;
        double bestScore = 0;
        public LinkedList<NNetwork1[]> zoo = new LinkedList<NNetwork1[]>();
        public int stepsTaken = 0;
        private double maxScoreTotal = 0;
        private int maxScoreTotalCount = 0;
        public Stats stats;
        private NNetwork1[] networks;
        private int currentNetworkIndex;
        //private int lastNetworkIndex = -1;
        private int trialIndex;
        Thread statusThread;
        IController c;
        //bool debug = false;
        public EvolutionLab() {
            statusThread = new Thread(p => {
                ((EvolutionLab)p).checkStatus();
            });
        }
        public void checkStatus() {
            //lastNetworkIndex = -1;
            while (true) {
                Thread.Sleep(600000);
                if (networks != null) {
                    //if (lastNetworkIndex == currentNetworkIndex) {
                    string score = "";
                    if (c != null) {
                        score = "_" + c.getScore() + "_" + ((AutoNNControllerV3)c).stepsLeft;
                    }
                    var fileName = ConsoleCopy.path_base + "_" + currentNetworkIndex + "_" + trialIndex + score + ".debug";
                    //debug = true;
                    try {
                        SerializeGeneration(networks, fileName, currentNetworkIndex);
                    }
                    catch (Exception ex) {
                        var fileWriter = new StreamWriter(ConsoleCopy.path_base + "_" + currentNetworkIndex + ".debug", append: true);
                        fileWriter.WriteLine(ex.ToString());
                        fileWriter.Close();
                    }
                    //}
                    //lastNetworkIndex = currentNetworkIndex;
                }
            }
        }
        public LinkedList<NNetwork1> evaluateGeneration(NNetwork1[] _networks, bool initLarge) {
            if (!statusThread.IsAlive) {
                statusThread.Start(this);
            }
            int stepsBefore = 0;
            networks = _networks;
            for (currentNetworkIndex = 0; currentNetworkIndex < networks.Length; ++currentNetworkIndex) {
                //if (currentNetworkIndex % 20 == 0) {
                //    Console.WriteLine("Milestone reached: " + currentNetworkIndex);
                //    //var fileStream = File.Create(ConsoleCopy.path_base + "_" + currentNetworkIndex + ".debug");
                //    //var fileWriter = new StreamWriter(fileStream);
                //    //SerializeNNetwork(networks[currentNetworkIndex], fileWriter);
                //    //fileWriter.Close();
                //}
                c = new AutoNNControllerV3(networks[currentNetworkIndex], initLarge);
                stats = c.getStats();
                if (currentNetworkIndex == 0) {
                    stepsBefore = stats.stepsTaken;
                }
                int score = 0;
                trialIndex = 0;
                for (; trialIndex < Config.TRIALS_PER_NETWORK; ++trialIndex) {
                    c.reloadModel();
                    c.play();
                    score += c.getScore();
                }
                trialIndex = 0;
                networks[currentNetworkIndex].survivedGenerations++;
                networks[currentNetworkIndex].CurrentScore = score / Config.TRIALS_PER_NETWORK;
            }
            Array.Sort(networks, (x, y) => ((int)(y.Score - x.Score)));
            int scoreCount = 0;
            for (int i = 0; i < networks.Length && networks[i].Score == networks[0].Score; ++i) {
                scoreCount++;
            }
            maxScoreTotalCount += scoreCount;
            maxScoreTotal += networks[0].Score * scoreCount;
            if (this.bestScore < networks[0].Score) {
                this.bestScore = networks[0].Score;
            }
            LinkedList<NNetwork1> bestNetworks = new LinkedList<NNetwork1>();
            for (int i = 0;
                //index out of bound safeguard
                i < networks.Length &&
                //include all with top score but no more than 10% of the population
                //((networks[i].MaxScore == networks[0].MaxScore && bestNetworks.Count < networks.Length / 10) || 
                (networks[i].Score >= (networks[0].Score * Config.CUTOFF_TO_REPRODUCE) ||
                //BUT let no fewer than 20 to reproduce
                bestNetworks.Count <
                Config.MIN_NUMBER_TO_REPRODUCE
                //Math.Ceiling(Math.Max(MIN_NUMBER_REPRODUCE, NEWCOMER_RATE * maxScoreCount))
                )
                ;
                ++i) {
                bestNetworks.AddLast(networks[i]);
            }
            Console.WriteLine("{0,6}{1,8}{2,4}{3,7}{4,9}{5,12}{6,20}", 
                networks[0].Score, networks[0].CurrentScore, networks[0].MaxScore, 
                networks[0].survivedGenerations, scoreCount, bestNetworks.Count, 
                (((double)stats.stepsTaken - stepsBefore) / networks.Length));
            currentNetworkIndex = -1;
            networks = null;
            //Console.WriteLine("\t" + networks[0].Score + "/" + networks[0].MaxScore + "/" + networks[0].survivedGenerations + "\t" + scoreCount + "\t" + bestNetworks.Count + "\t" + (((double)stats.stepsTaken - stepsBefore) / networks.Length));
            return bestNetworks;
        }
        /// <summary>
        ///  Create a generation of random Neural networks
        /// </summary>
        /// <param name="size">number of networs in generation</param>
        /// <param name="priorGenerationChildren">include all these networks in new generation</param>
        /// <returns></returns>
        public NNetwork1[] getGeneration(int size, NNetwork1[] priorGenerationChildren = null) {
            NNetwork1[] res = new NNetwork1[size];
            int i = 0;
            if (priorGenerationChildren != null) {
                for (; i < priorGenerationChildren.Length; ++i) {
                    res[i] = priorGenerationChildren[i];
                }
            }
            for (; i < res.Length; ++i) {
                res[i] = new NNetwork1(Config.INPUT_COUNT, Config.DEFAULT_LAYER2_COUNT, Config.DEFAULT_LAYER3_COUNT, Config.OUTPUT_COUNT);
            }
            return res;
        }
        public NNetwork1[] getGeneration(string path, int size = -1, bool expand = true) {
            NNetwork1[] res = null;
            var fileStream = File.OpenRead(path);
            var fileReader = new StreamReader(fileStream);
            int sampleSize = int.Parse(fileReader.ReadLine().Split('\t')[0]);
            if (size > 0 && size < sampleSize) {
                sampleSize = size;
            }
            res = new NNetwork1[sampleSize];
            fileReader.ReadLine();
            int inputCount = int.Parse(fileReader.ReadLine().Split('\t')[0]);
            int layer2Count = int.Parse(fileReader.ReadLine().Split('\t')[0]);
            int layer3Count = int.Parse(fileReader.ReadLine().Split('\t')[0]);
            int outputCount = int.Parse(fileReader.ReadLine().Split('\t')[0]);
            Config.INPUT_COUNT = inputCount;
            Config.OUTPUT_COUNT = outputCount;
            if (inputCount != Config.INPUT_COUNT || outputCount != Config.OUTPUT_COUNT || layer2Count != Config.DEFAULT_LAYER2_COUNT || layer3Count != Config.DEFAULT_LAYER3_COUNT) {
                throw new InvalidDataException("File contains neural network with I/O count of " + inputCount + "/" + layer2Count + "/" + layer3Count + "/" + outputCount +
                    " while program is set to use I/O count of " + Config.INPUT_COUNT + "/" + Config.DEFAULT_LAYER2_COUNT + "/" + Config.DEFAULT_LAYER3_COUNT + "/" + Config.OUTPUT_COUNT);
            }
            int i = 0;
            for (i = 0; i < sampleSize; ++i) {
                if (fileReader.EndOfStream)
                    break;
                int maxScore = int.Parse(fileReader.ReadLine().Split('\t')[0]);
                res[i] = new NNetwork1(inputCount, layer2Count, layer3Count, outputCount, fileReader);
            }
            if (fileReader != null) {
                fileReader.Close();
            }
            if (fileStream != null) {
                fileStream.Close();
            }
            if (expand) {
                Array.Resize<NNetwork1>(ref res, i);
                //res = getGeneration(size: Config.NETWORKS_PER_GENERATION, priorGenerationChildren: res);
                res = getGeneration(size: (int)(res.Length * Config.RANDOM_ADDITION_TO_EACH_GENERATION),
                            priorGenerationChildren: res);
                res = reproduce(parents: res, size: Config.NETWORKS_PER_GENERATION);
            }
            return res;
        }
        public void runTrial(string fileWithInitialGeneration = null, int topNetworksCounter = -1, NNetwork1[] initialNetwork = null) {
            NNetwork1[] res = null;
            Config.TRIAL_OUTCOME_SIZE = 40;

            //Config.MUTATION_RATE = 0.2;
            //Config.RANDOM_ADDITION_TO_EACH_GENERATION = 1;
            //Config.TRIALS_PER_NETWORK = 1;
            //Config.GENERATIONS = 80;
            //Config.CUTOFF_TO_REPRODUCE = 0.8;
            //Config.REPRODUCTION_RANDOMNESS = ReproductionRandomnessMethod.SKEWED_TO_BEGINNING;
            //res = runExperiment(fileWithInitialGeneration: fileWithInitialGeneration, topNetworksCounter: topNetworksCounter, initialNetwork: res);

            //Config.MUTATION_RATE = 0.1;
            //Config.RANDOM_ADDITION_TO_EACH_GENERATION = 1;
            //Config.TRIALS_PER_NETWORK = 1;
            //Config.GENERATIONS = 80;
            //Config.CUTOFF_TO_REPRODUCE = 0.9;
            //Config.REPRODUCTION_RANDOMNESS = ReproductionRandomnessMethod.SKEWED_TO_BEGINNING;
            //res = runExperiment(fileWithInitialGeneration: fileWithInitialGeneration, topNetworksCounter: topNetworksCounter, initialNetwork: res);

            //Config.MUTATION_RATE = 0.05;
            //Config.RANDOM_ADDITION_TO_EACH_GENERATION = 1;
            //Config.TRIALS_PER_NETWORK = 1;
            //Config.GENERATIONS = 80;
            //Config.CUTOFF_TO_REPRODUCE = 0.9;
            //Config.REPRODUCTION_RANDOMNESS = ReproductionRandomnessMethod.SKEWED_TO_BEGINNING;
            //res = runExperiment(fileWithInitialGeneration: fileWithInitialGeneration, topNetworksCounter: topNetworksCounter, initialNetwork: res);

            Config.MUTATION_RATE = 0.1;
            Config.RANDOM_ADDITION_TO_EACH_GENERATION = 1;
            Config.TRIALS_PER_NETWORK = 3;
            Config.GENERATIONS = 200;
            Config.NETWORKS_PER_GENERATION = 3000;
            Config.TRIAL_OUTCOME_SIZE = 100;
            Config.CUTOFF_TO_REPRODUCE = 0.9;
            Config.STEPS_PER_FOOD = 400;
            Config.INIT_LARGE = false;
            Config.REPRODUCTION_RANDOMNESS = ReproductionRandomnessMethod.SKEWED_TO_BEGINNING;
            res = runExperiment(fileWithInitialGeneration: fileWithInitialGeneration, topNetworksCounter: topNetworksCounter, initialNetwork: res, Config.INIT_LARGE);

            //Config.MUTATION_RATE = 0.1;
            //Config.TRIALS_PER_NETWORK = 4;
            //Config.GENERATIONS = 60;
            //Config.CUTOFF_TO_REPRODUCE = 0.8;
            //res = runExperiment(topNetworksCounter: topNetworksCounter, initialNetwork: res);

            //Config.MUTATION_RATE = 0.05;
            //Config.TRIALS_PER_NETWORK = 1;
            //Config.GENERATIONS = 200;
            //Config.CUTOFF_TO_REPRODUCE = 0.9;
            //res = runExperiment(topNetworksCounter: topNetworksCounter, initialNetwork: res);

            //Config.MUTATION_RATE = 0.1;
            //Config.TRIALS_PER_NETWORK = 1;
            //Config.GENERATIONS = 200;
            //Config.CUTOFF_TO_REPRODUCE = 1;
            //res = runExperiment(topNetworksCounter: topNetworksCounter, initialNetwork: res);

            //Config.MUTATION_RATE = 0.1;
            //Config.TRIALS_PER_NETWORK = 4;
            //Config.GENERATIONS = 60;
            //Config.CUTOFF_TO_REPRODUCE = 0.95;
            //res = runExperiment(topNetworksCounter: topNetworksCounter, initialNetwork: res);

            //Config.MUTATION_RATE = 0.05;
            //Config.TRIALS_PER_NETWORK = 4;
            //Config.GENERATIONS = 60;
            //Config.CUTOFF_TO_REPRODUCE = 1;
            //res = runExperiment(topNetworksCounter: topNetworksCounter, initialNetwork: res);
        }
        public NNetwork1[] runExperiment(string fileWithInitialGeneration, int topNetworksCounter, NNetwork1[] initialNetwork, bool initLarge) {
            Console.WriteLine(    "Experiment No:                      " + experimentCounter);
            Console.WriteLine(Config.toString());
            NNetwork1[] generation;
            if (initialNetwork != null) {
                generation = initialNetwork;
                Console.WriteLine("Initial Generation preloaded. Score:" + initialNetwork[0].MaxScore);
            }
            else if (fileWithInitialGeneration != null) {
                Console.WriteLine("Initial Generation:                 {0}", fileWithInitialGeneration);
                if (topNetworksCounter == -1) {
                    Console.WriteLine("Use top X matrices:                 all");
                }
                else {
                    Console.WriteLine("Use top X matrices:                 {0}", topNetworksCounter);
                }
                generation = getGeneration(fileWithInitialGeneration, topNetworksCounter);
            }
            else {
                Console.WriteLine("Initial Generation:                 random");
                generation = getGeneration(Config.NETWORKS_PER_GENERATION);
            }
            //Console.WriteLine("Gen\tScore/HI/Gen#\tScore#\tSurvivors#\tSteps per game");
            Console.WriteLine("{0,5}{1,6}{2,8}{3,4}{4,7}{5,9}{6,12}{7,20}", "Gen", "Score", "Current", "HI", "Gen#", "Score#", "Survivors#", "Steps per game");
            int pageSize = 10;
            for (int i = 0; i < Config.GENERATIONS; ++i) {
                Console.Write("{0,5}", (i + 1));
                var bestNetowrks = evaluateGeneration(generation, initLarge);
                zoo.AddLast(bestNetowrks.ToArray<NNetwork1>());
                if (bestNetowrks.First().Score == 0) {
                    generation = getGeneration(Config.NETWORKS_PER_GENERATION);
                    Console.WriteLine("Throwing away this generation.");
                }
                else {
                    generation = getGeneration(size: (int)(bestNetowrks.Count * Config.RANDOM_ADDITION_TO_EACH_GENERATION), 
                        priorGenerationChildren: bestNetowrks.ToArray<NNetwork1>());
                    generation = reproduce(parents: generation, size: Config.NETWORKS_PER_GENERATION);
                    //generation = reproduce(bestNetowrks, 9 * Config.NETWORKS_PER_GENERATION / 10);
                    //generation = getGeneration(priorGenerationChildren: generation);
                }
                if(i % pageSize == (pageSize - 1)) {
                    Console.WriteLine("Cumulative Score of the last " + pageSize + " generations:" + maxScoreTotal / maxScoreTotalCount);
                    maxScoreTotal = 0;
                    maxScoreTotalCount = 0;
                    SerializeGeneration(zoo.Last(), ConsoleCopy.path_base + "_" +
                        experimentCounter.ToString().PadLeft(2, '0') + "_" +
                        i.ToString().PadLeft(3, '0') + ".out");
                }
            }
            stats.mutationsCounter = Matrix.mutatedCounter;
            Matrix.mutatedCounter = 0;
            SerializeTheBest(zoo, ConsoleCopy.path_base + ".out");
            Console.WriteLine("Best score across all generations:" + this.bestScore);
            experimentCounter++;
            return zoo.Last();
        }
        private void SerializeGeneration(NNetwork1[] generation, string path, int size = -1) {
            if (generation == null) { return; }
            try {
                var fileStream = File.Create(path);
                var fileWriter = new StreamWriter(fileStream);
                fileWriter.AutoFlush = true;
                fileWriter.WriteLine(Math.Min(Config.TRIAL_OUTCOME_SIZE, generation.Length) + "\tmatrices");
                fileWriter.WriteLine(this.bestScore + "\tbest score");
                fileWriter.WriteLine(generation[0].inputCount + "\tinput count");
                fileWriter.WriteLine(generation[0].layer2Count + "\tlayer2 count");
                fileWriter.WriteLine(generation[0].layer3Count + "\tlayer3 count");
                fileWriter.WriteLine(generation[0].outputCount + "\toutput count");
                int lastIndexToSerialize;
                if (size == -1) {
                    lastIndexToSerialize = Math.Min(Config.TRIAL_OUTCOME_SIZE, generation.Length);
                }
                else {
                    lastIndexToSerialize = Math.Min(size, generation.Length);
                }
                for (int i = 0; i < lastIndexToSerialize && i < generation.Length; ++i) {
                    //serialize only unique networks - check if a duplicate has already been serialized
                    bool duplicate = false;
                    for (int j = 0; j < i; ++j) {
                        if (generation[j].Equals(generation[i])) {
                            duplicate = true;
                            break;
                        }
                    }
                    if (!duplicate) {
                        SerializeNNetwork(generation[i], fileWriter);
                    }
                    else {
                        //bump the upper index up since current network is a duplicate
                        lastIndexToSerialize++;
                    }
                }
                fileWriter.Flush();
                fileWriter.Close();
            }
            catch (Exception e) {
                Console.WriteLine("Cannot open file for writing");
                Console.WriteLine(e.Message);
                return;
            }
        }
        public void SerializeTheBest(LinkedList<NNetwork1[]> zoo, string path) {
            NNetwork1[] allGenerations = { };
            foreach(var gen in zoo) {
                allGenerations = allGenerations.Concat(gen).ToArray();
            }
            Array.Sort(allGenerations, (x, y) => ((int)(y.MaxScore - x.MaxScore)));
            
            SerializeGeneration(allGenerations, path);
        }
        public void SerializeNNetwork(NNetwork1 nnetwork, StreamWriter fileWriter) {
            fileWriter.WriteLine(nnetwork.MaxScore + "\tmax score\t" + nnetwork.survivedGenerations + "\tgenerations old");
            fileWriter.WriteLine(nnetwork.weights1.ToString());
            fileWriter.WriteLine(nnetwork.weights2.ToString());
            fileWriter.WriteLine(nnetwork.weights3.ToString());
        }
        public NNetwork1[] reproduce(LinkedList<NNetwork1> parents, int size) {
            return reproduce(parents.ToArray<NNetwork1>(), size);
        }
        public NNetwork1[] reproduce(NNetwork1[] parents, int size) {
        NNetwork1[] res = new NNetwork1[size];
        int i = 0;
        //include parents
        for (; i < parents.Length /*&& i < networksPerGeneration*/; ++i) {
            res[i] = parents.ElementAt<NNetwork1>(i);
        }
        for (; i < size; ++i) {
            int first = random(parents.Length), second = random(parents.Length);
            //int first = rand.Next(parents.Length), second = rand.Next(parents.Length);
            res[i] = new NNetwork1(parents.ElementAt<NNetwork1>(first), parents.ElementAt<NNetwork1>(second));
        }
        return res;
        }
        public static int random(int maxValue) {
            //return Math.Abs(rand.Next(maxValue + 1) - rand.Next(maxValue + 1));
            if (Config.REPRODUCTION_RANDOMNESS == ReproductionRandomnessMethod.SKEWED_TO_BEGINNING) {
                return (int)Math.Floor(Math.Abs(rand.NextDouble() - rand.NextDouble()) * maxValue);
            }
            else if(Config.REPRODUCTION_RANDOMNESS == ReproductionRandomnessMethod.EVENLY_DISTRIBUTES) {
                return rand.Next(maxValue);
            }
            else {
                throw new InvalidOperationException("Cannot determine a random selectin method for choosing parents for reproduction.");
            }
        }
    }
}
