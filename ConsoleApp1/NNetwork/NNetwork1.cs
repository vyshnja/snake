﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace ConsoleApp1.NNetwork {
    public enum CrossoverMode {
        INCOMING_IN_BLOCKS,
        OUTCOMING_IN_BLOCKS,
        ALTERNATING
    }
    class NNetwork1 {
        static private Random rand = new Random();
        public int inputCount;
        public int layer2Count;
        public int layer3Count;
        public int outputCount;
        private double maxScore;
        private double currentScore;
        private int scoreCount;
        private double scoreSum;
        public int survivedGenerations = 0;
        public int maxScoreGeneration = 0;
        //public static CrossoverMode crossoverMode;
        public double MaxScore {
            get { return maxScore; }
            private set { }
        }
        public double CurrentScore {
            get { return currentScore; }
            set { 
                currentScore = value;
                scoreCount++;
                scoreSum += currentScore;
                if (currentScore >= maxScore) {
                    maxScore = currentScore;
                    maxScoreGeneration = survivedGenerations;
                }
            }
        }
        public int Score {
            get { return (int) (currentScore + Math.Max((MaxScore - currentScore - (survivedGenerations - maxScoreGeneration + 1) / 3), 0));
            //get { return (int)currentScore;
            }
            private set { }
        }
        public Matrix weights1, weights2, weights3;
        public static Matrix weights1Total, weights2Total, weights3Total;
        public static int weights1Counter = 0;
        /// <summary>
        /// Initialize network to the default values for neuron layers sizes
        /// defined by class constantc
        /// </summary>
        public NNetwork1(): this(Config.INPUT_COUNT, Config.DEFAULT_LAYER2_COUNT, Config.DEFAULT_LAYER3_COUNT, Config.OUTPUT_COUNT) {}
        public NNetwork1(int inputCount, int layer2Count, int layer3Count, int outputCount, StreamReader cin) {
            this.inputCount = inputCount;
            this.layer2Count = layer2Count;
            this.layer3Count = layer3Count;
            this.outputCount = outputCount;
            weights1 = new Matrix(this.layer2Count, this.inputCount, cin);
            if (weights1Total == null) {
                weights1Total = new Matrix(this.layer2Count, this.inputCount);
            }
            weights1Total.add(weights1);
            weights1Counter++;
            weights2 = new Matrix(this.layer3Count, this.layer2Count, cin);
            if (weights2Total == null) {
                weights2Total = new Matrix(this.layer2Count, this.inputCount);
            }
            weights2Total.add(weights2);
            weights3 = new Matrix(this.outputCount, this.layer3Count, cin);
            if (weights3Total == null) {
                weights3Total = new Matrix(this.layer2Count, this.inputCount);
            }
            weights3Total.add(weights3);
        }
        /// <summary>
        /// Initialize network to the specified values for neuron layers sizes
        /// defined by class constantc
        /// </summary>
        public NNetwork1(int inputCount, int layer2Count, int layer3Count, int outputCount) {
            this.inputCount = inputCount;
            this.layer2Count = layer2Count;
            this.layer3Count = layer3Count;
            this.outputCount = outputCount;
            weights1 = new Matrix(this.layer2Count, this.inputCount);
            if (weights1Total == null) {
                weights1Total = new Matrix(this.layer2Count, this.inputCount);
            }
            weights1Total.add(weights1);
            weights1Counter++;
            weights2 = new Matrix(this.layer3Count, this.layer2Count);
            if (weights2Total == null) {
                weights2Total = new Matrix(this.layer2Count, this.inputCount);
            }
            weights2Total.add(weights2);
            weights3 = new Matrix(this.outputCount, this.layer3Count);
            if (weights3Total == null) {
                weights3Total = new Matrix(this.layer2Count, this.inputCount);
            }
            weights3Total.add(weights3);
        }
        /// <summary>
        /// Create empty network of the same size as source network
        /// defined by class constantc
        /// </summary>
        public NNetwork1(NNetwork1 parent1, NNetwork1 parent2) {
            this.inputCount = parent1.inputCount;
            this.layer2Count = parent1.layer2Count;
            this.layer3Count = parent1.layer3Count;
            this.outputCount = parent1.outputCount;
            this.weights1 = Matrix.swap(parent1.weights1, parent2.weights1);
            weights1Total.add(weights1);
            this.weights2 = Matrix.swap(parent1.weights2, parent2.weights2);
            weights2Total.add(weights2);
            this.weights3 = Matrix.swap(parent1.weights3, parent2.weights3);
            weights3Total.add(weights3);
        }
        public Vector run(Vector input) {
            Vector res;
            res = weights1.multiply(input);
            res = weights2.multiply(res);
            res = weights3.multiply(res);
            return res;
        }
        public override bool Equals(object obj) {
            NNetwork1 second = (NNetwork1)obj;
            return this.weights1.Equals(second.weights1) && this.weights2.Equals(second.weights2) && this.weights3.Equals(second.weights3);
        }
        public override int GetHashCode() {
            return this.weights1.GetHashCode() + this.weights2.GetHashCode() + this.weights3.GetHashCode();
        }

    }
}
