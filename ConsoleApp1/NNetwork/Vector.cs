﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1.NNetwork {
    class Vector {
        public double[] vs { get; set; }
        public int length { get; private set; }
        public Vector(int n) {
            vs = new double[n];
            length = n;
        }
        public Vector(Vector src) {
            vs = new double[src.vs.Length];
            length = src.vs.Length;
            for(int i = 0; i < src.vs.Length; ++i) {
                vs[i] = src.vs[i];
            }
        }
        public void merge(Vector src, int offset) {
            if (this.vs.Length < src.vs.Length + offset) {
                throw new IndexOutOfRangeException("Target vector is too small (" + this.vs.Length + 
                    ")to merge vector of size" + src.vs.Length + " at offset " + offset);
            }
            int currentOffset = offset;
            foreach (var v in src.vs) {
                this.vs[currentOffset++] = v;
            }
        }
        public int maxIndex() {
            double max = Double.MinValue;
            int index = -1;
            for (int i = 0; i < this.vs.Length; i++) {
                if(max < this.vs[i]){
                    index = i;
                    max = this.vs[i];
                }
            }
            if(index == -1){
                throw new EntryPointNotFoundException("Failed to find max element in vector");
            }
            return index;
        }
        public void add(Vector second) {
            if (this.vs.Length != second.vs.Length) {
                throw new InvalidOperationException("Cannot add vectors of different length: " +
                    this.vs.Length + " and " + second.vs.Length);
            }
            for (int i = 0; i < this.vs.Length; ++i) {
                this.vs[i] += second.vs[i];
            }
        }
        public Vector divideBy(int divisor) {
            Vector res = new Vector(this);
            if (divisor == 0) {
                throw new DivideByZeroException("Cannot divide vector by zero1" );
            }
            for (int i = 0; i < res.vs.Length; ++i) {
                res.vs[i] /= divisor;
            }
            return res;
        }
        public override string ToString() {
            string res = "";
            foreach(var v in vs) {
                res += v + ",";
            }
            return res;
        }
    }
}
