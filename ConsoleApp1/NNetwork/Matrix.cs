﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace ConsoleApp1.NNetwork {
    class Matrix {
        static private Random rand = new Random();
        public static int mutatedCounter = 0;
        public double[,] ws { get; private set; }
        public int height { get; private set; }
        public int width { get; private set; }
        public Matrix(int _height, int _width, bool randomize = true) {
            ws = new double[_height, _width];
            height = _height;
            width = _width;
            if (randomize) {
                Matrix.initRandom(this);
            }
        }
        public Matrix(int _height, int _width, StreamReader cin) {
            ws = new double[_height, _width];
            height = _height;
            width = _width;
            for (int i = 0; i < this.ws.GetLength(0); ++i) {
                string[] line = cin.ReadLine().Split('\t');
                for (int j = 0; j < this.ws.GetLength(1); ++j) {
                    this.ws[i, j] = double.Parse(line[j]);
                }
            }
            cin.ReadLine();
        }
        private static double getNewWeight() {
            return Math.Round(rand.NextDouble(), 4) * 2 - 1;
        }
        private static void initRandom(Matrix matrix) {
            for (int i = 0; i < matrix.ws.GetLength(0); ++i) {
                for (int j = 0; j < matrix.ws.GetLength(1); ++j) {
                    matrix.ws[i, j] = getNewWeight();
                }
            }
        }
        public void add(Matrix matrix) {
            for (int i = 0; i < matrix.ws.GetLength(0); ++i) {
                for (int j = 0; j < matrix.ws.GetLength(1); ++j) {
                    this.ws[i, j] += matrix.ws[i, j];
                }
            }
        }
        public override string ToString() {
            string res = "";
            for (int i = 0; i < this.ws.GetLength(0); ++i) {
                for (int j = 0; j < this.ws.GetLength(1); ++j) {
                    res += Math.Round(this.ws[i, j], 4) + "\t";
                }
                res += Environment.NewLine;
            }
            return res;
        }
        private static double Exp(double val) {
            long tmp = (long)(1512775 * val + 1072632447);
            return BitConverter.Int64BitsToDouble(tmp << 32);
        }
        //normializarion function
        public static double sigmoid(double value) {
            //float k = (float)Math.Exp(value);
            //return Math.Round(k / (1.0f + Math.Exp(value)), 2);
            //return 1.0f / (1.0f + (float)Math.Exp(-value));
            return (double)(1.0 / (1.0 + Math.Pow(Math.E, -value)));
            //double y = -(1 / (value + 1)) + 1;
            //return y;
        }
        //private static double sigmoid(double x) {
        //    double y = -(1 / (x + 1)) + 1 ;
        //    return y;
        //}
        /// <summary>
        /// Create connections between neuron layers by randomly swapping connections of two 
        /// existing connection matrices. A unit of swapping is ALL links from a single neuron
        /// to all neurons in the next layer, i.e., all connections from a neuron in parent
        /// are preserved in children
        /// </summary>
        /// <param name="parent1"></param>
        /// <param name="parent2"></param>
        /// <param name="child1"></param>
        /// <param name="child2"></param>
        public static void swap(Matrix parent1, Matrix parent2, Matrix child1, Matrix child2) {
            for (int i = 0; i < parent1.ws.GetLength(0); ++i) {
                Matrix first, second;
                if (Convert.ToBoolean(rand.Next(2))) {
                    first = parent1;
                    second = parent2;
                }
                else {
                    first = parent2;
                    second = parent1;
                }
                for (int j = 0; j < parent1.ws.GetLength(1); ++j) {
                    child1.ws[i, j] = first.ws[i, j];
                    child2.ws[i, j] = second.ws[i, j];
                }
            }
        }
        public static Matrix swap(Matrix parent1, Matrix parent2) {
            switch (Config.CROSSOVER_METHOD) {
                case CrossoverMode.INCOMING_IN_BLOCKS:  return swapGroupIncomingInBlocks(parent1, parent2); 
                case CrossoverMode.OUTCOMING_IN_BLOCKS: return swapGroupOutgoingInBlocks(parent1, parent2);
                case CrossoverMode.ALTERNATING: 
                    if(rand.Next(2) == 1)
                        return swapGroupIncomingInBlocks(parent1, parent2);
                    else
                        return swapGroupOutgoingInBlocks(parent1, parent2);

                default: throw new InvalidOperationException("Invalid crossover method:" + Config.CROSSOVER_METHOD);
            }
            //return swapGroupIncomingInBlocks(parent1, parent2, 3);
            //return swapGroupIncoming(parent1, parent2);
            //return swapGroupOutgoing(parent1, parent2);
        }
        /// <summary>
        /// when swapping, treat all values coming into the neuron as a single unit, i.e.,
        /// don't split them
        /// </summary>
        /// <param name="parent1"></param>
        /// <param name="parent2"></param>
        /// <returns></returns>
        public static Matrix swapGroupOutgoing(Matrix parent1, Matrix parent2) {
            Matrix child = new Matrix(parent1.height, parent1.width, false);
            for (int i = 0; i < parent1.ws.GetLength(0); ++i) {
                Matrix chosen;
                if (Convert.ToBoolean(rand.Next(2))) {
                    chosen = parent1;
                }
                else {
                    chosen = parent2;
                }
                for (int j = 0; j < parent1.ws.GetLength(1); ++j) {
                    child.ws[i, j] = chosen.ws[i, j];
                }
            }
            return child;
        }
        /// <summary>
        /// when swapping, treat all values coming into the neuron as a single unit, i.e.,
        /// don't split them
        /// </summary>
        /// <param name="parent1"></param>
        /// <param name="parent2"></param>
        /// <returns></returns>
        public static Matrix swapGroupOutgoingInBlocks(Matrix parent1, Matrix parent2) {
            Matrix child = new Matrix(parent1.height, parent1.width, false);
            Matrix[] parents = { parent1, parent2 };
            int chosenParent = rand.Next(2);
            Matrix chosen = parents[chosenParent];
            for (int i = 0; i < parent1.ws.GetLength(0);) {
                int crossoverPoint = rand.Next(parent1.ws.GetLength(0) - i) + i + 1;
                for (; i < parent1.ws.GetLength(0) && i < crossoverPoint; ++i) {
                    for (int j = 0; j < parent1.ws.GetLength(1); ++j) {
                        child.ws[i, j] = chosen.getWeight(i, j);
                    }
                }
                chosenParent = ++chosenParent % 2;
                chosen = parents[chosenParent];
            }
            return child;
        }
        /// <summary>
        /// when swapping, treat all values coming out of the neuron as a single unit, i.e.,
        /// don't split them
        /// </summary>
        /// <param name="parent1"></param>
        /// <param name="parent2"></param>
        /// <returns></returns>
        public static Matrix swapGroupIncoming(Matrix parent1, Matrix parent2) {
            Matrix child = new Matrix(parent1.height, parent1.width, false);
            for (int j = 0; j < parent1.ws.GetLength(1); ++j) {
                Matrix chosen;
                if (Convert.ToBoolean(rand.Next(2))) {
                    chosen = parent1;
                }
                else {
                    chosen = parent2;
                }
                for (int i = 0; i < parent1.ws.GetLength(0); ++i) {
                    child.ws[i, j] = chosen.ws[i, j];
                }
            }
            return child;
        }
        /// <summary>
        /// when swapping, treat all values coming out of the neuron as a single unit, i.e.,
        /// don't split them
        /// </summary>
        /// <param name="parent1"></param>
        /// <param name="parent2"></param>
        /// <returns></returns>
        public double getWeight(int i, int j) {
            if (rand.NextDouble() < Config.MUTATION_RATE) {
                mutatedCounter++;
                return getNewWeight();
            }
            else {
                return this.ws[i, j];
            }
        }
        public static Matrix swapGroupIncomingInBlocks(Matrix parent1, Matrix parent2) {
            Matrix child = new Matrix(parent1.height, parent1.width, false);
            Matrix[] parents = { parent1, parent2 };
            int chosenParent = rand.Next(2);
            Matrix chosen = parents[chosenParent];
            for (int j = 0; j < parent1.ws.GetLength(1);) {
                int crossoverPoint = rand.Next(parent1.ws.GetLength(1) - j) + j + 1;
                for (; j < parent1.ws.GetLength(1) && j < crossoverPoint; ++j) {
                    for (int i = 0; i < parent1.ws.GetLength(0); ++i) {
                        child.ws[i, j] = chosen.getWeight(i, j);
                    }
                }
                chosenParent = ++chosenParent % 2;
                chosen = parents[chosenParent];
            }
            return child;
        }
        /// <summary>
        /// when swapping, treat all values coming out of the neuron as a single unit, i.e.,
        /// don't split them
        /// </summary>
        /// <param name="parent1"></param>
        /// <param name="parent2"></param>
        /// <returns></returns>
        public static Matrix swapGroupIncomingInBlocks(Matrix parent1, Matrix parent2, int numberOfCrossOvers) {
            Matrix child = new Matrix(parent1.height, parent1.width, false);
            int[] crossoverPoints = new int[numberOfCrossOvers + 1];
            for (int i = 0; i < numberOfCrossOvers; ++i) {
                crossoverPoints[i] = rand.Next(parent1.ws.GetLength(1)) * 3 / 4 + rand.Next(parent1.ws.GetLength(1)) / 4;
            }
            crossoverPoints[numberOfCrossOvers] = parent1.ws.GetLength(1);
            Array.Sort(crossoverPoints);
            Matrix[] parents = { parent1, parent2 };
            int chosenParent = rand.Next(2);
            Matrix chosen = parents[chosenParent];
            int j = 0;
            for (int c = 0; c < crossoverPoints.Length; ++c) { 
                for (; j < parent1.ws.GetLength(1) && j < crossoverPoints[c]; ++j) {
                    for (int i = 0; i < parent1.ws.GetLength(0); ++i) {
                        child.ws[i, j] = chosen.ws[i, j];
                    }
                }
                chosenParent = ++chosenParent % 2;
                chosen = parents[chosenParent];
            }
            return child;
        }
        public Vector multiply(Vector v) {
            if (this.width != v.length) {
                throw new InvalidOperationException("Input vector length (" +
                    v.length + ") differs from matrix width(" + this.width + ")");
            }
            Vector res = new Vector(this.height);
            int height = this.ws.GetLength(0);
            int width = this.ws.GetLength(1);
            for (int i = 0; i < height; ++i) {
                res.vs[i] = 0;
                for (int j = 0; j < width; ++j) {
                    res.vs[i] += this.ws[i, j] * v.vs[j];
                }
                res.vs[i] = sigmoid(res.vs[i]);
            }
            return res;
        }
        public override int GetHashCode() {
            unchecked {
                if (this.ws== null) {
                    return 0;
                }
                int hash = 17;
                foreach (var element in this.ws) {
                    hash = hash * 31 + element.GetHashCode();
                }
                return hash;
            }
        }
        public override bool Equals(object obj) {
            Matrix second = (Matrix)obj;
            if (second.height != this.height || second.width != this.width)
                return false;
            for (int i = 0; i < height; ++i) {
                for (int j = 0; j < width; ++j) {
                    if (this.ws[i, j] != second.ws[i, j])
                        return false;
                }
            }
            return true;
        }
    }
}
