﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ConsoleApp1.NNetwork {
    class Stats {
        public Stats(int input_count, int output_count) {
            resultingNeuronIndex = new ulong[output_count];
            totalSnakeState = new Vector(input_count);
            totalResultingVector = new Vector(output_count);
            ranOutOfStepsCounter = 0;
            gamesWonCounter = 0;
        }
        public int stepsTaken { get; set; }
        public ulong[] resultingNeuronIndex { get; set; }
        public Vector totalSnakeState { get; set; }
        public Vector totalResultingVector { get; set; }
        public Vector totalState { get; set; }
        public int ranOutOfStepsCounter;
        public int mutationsCounter;
        public int gamesWonCounter;
    }
}
