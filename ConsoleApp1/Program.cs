﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Threading;
namespace ConsoleApp1 {
    using ConsoleApp1.NNetwork;
    using System.Diagnostics;

    class Program {
        static void Main(string[] args) {
            var directory = new DirectoryInfo(Directory.GetCurrentDirectory());
            var allFiles = directory.GetFiles("*.out");
            Array.Sort(allFiles, (x, y) => (int)((y.LastWriteTime - x.LastWriteTime).TotalSeconds));
            string outFile = allFiles[0].FullName;
            allFiles = directory.GetFiles("*.debug");
            Array.Sort(allFiles, (x, y) => (int)((y.LastWriteTime - x.LastWriteTime).TotalSeconds));
            string debugFile = "";
            if (allFiles.Length > 0) {
                debugFile = allFiles[0].FullName;
            }
            //file = Directory.GetCurrentDirectory() + "\\" + file + ".out";
            Console.WriteLine("What to run?");
            Console.WriteLine("1 - Run trial with random seed population");
            Console.WriteLine("2 - Run trial with seed population from the file:" + outFile);
            Console.WriteLine("3 - Run demo with the best snake from the file:" + outFile);
            Console.WriteLine("4 - Run FAST demo with the best snake from the file:" + outFile);
            if (allFiles.Length > 0) {
                Console.WriteLine("5 - Run demo with the latest tested snake from debug file:" + debugFile);
            }
            string choice = Console.ReadLine();
            Console.Clear();
            Console.WriteLine("Running...");
            Stopwatch sw = new Stopwatch();
            
            sw.Start();
            switch (choice) {
                case "1": neuronNetworkLabControl(); break;
                case "2": neuronNetworkLabControl(outFile); break;
                //case "2": neuronNetworkLabControl(Directory.GetCurrentDirectory() + "\\" + file + ".out"); break;
                case "3": loadFromFileNeuronNetworkAndDemoControl(outFile, 10);break;
                case "4": loadFromFileNeuronNetworkAndDemoControl(outFile, 0);break;
                case "5": loadFromFileNeuronNetworkAndDemoControl(debugFile, 1, -1);break;
                //case "3": loadFromFileNeuronNetworkAndDemoControl(Directory.GetCurrentDirectory() + "\\" + file + ".out");break;
            //randomNeuronNetworksControl();
            //dummyDirectAlgControl();
            //dummySpiralAlgControl();
            //manualControl();
            }
            sw.Stop();
            Console.WriteLine("Elapsed={0}", sw.Elapsed);
        }
        private static void neuronNetworkLabControl(string fileWithInitialGeneration = null, int topNetworksCounter = -1) {
            using (var cc = new ConsoleCopy()) {
                EvolutionLab lab = new EvolutionLab();
                lab.runTrial(fileWithInitialGeneration, topNetworksCounter);
                Console.WriteLine("===Stats===");
                Console.WriteLine("Mutations per generation:" + (lab.stats.mutationsCounter / (Config.GENERATIONS)));
                Console.WriteLine("Games won:" + lab.stats.gamesWonCounter);
                Console.WriteLine("Count by fired neuron");
                for (int i = 0; i < lab.stats.resultingNeuronIndex.Length; ++i) {
                    Console.Write(i + ":" + (lab.stats.resultingNeuronIndex[i] * 100 / (ulong)lab.stats.stepsTaken) + "%; ");
                }
                Console.WriteLine();
                Console.WriteLine("Average of resulting vectors");
                Vector averageResultingVector = new Vector(lab.stats.totalResultingVector.divideBy(lab.stats.stepsTaken));
                for (int i = 0; i < averageResultingVector.vs.Length; ++i) {
                    Console.Write(i + ":" + Math.Round(averageResultingVector.vs[i], 4) + ";");
                }
                Console.WriteLine();
                Console.WriteLine("Average of input vectors");
                Vector averageSnakeState = new Vector(lab.stats.totalSnakeState.divideBy(lab.stats.stepsTaken));
                for (int i = 0; i < averageSnakeState.vs.Length; ++i) {
                    if (i % 3 == 0) {
                        Console.Write("\n" + i + "\t");
                    }
                    Console.Write(Math.Round(averageSnakeState.vs[i], 4) + "\t");
                }
                Console.WriteLine();
                Console.WriteLine(lab.stats.ranOutOfStepsCounter + " games ran out of steps");
                Console.WriteLine();
            }
        }
        private static void randomNeuronNetworksControl() {
            int maxScore = 0;
            Console.WriteLine("Launching games");
            int i = 0;
            while (maxScore < 4){
                if (i++ % 2000 == 0) {
                    Console.WriteLine("Running game No " + i);
                }
                Model m = new Model(20, 20, 0);
                IController c = new AutoNNController(m);
                View v = new View(m, 300);
                c.play();
                if (c.getScore() > maxScore) {
                    maxScore = c.getScore();
                    Console.WriteLine("new high score:" + maxScore + "/game No " + i);
                }
            }
            Console.WriteLine("Max score:" + maxScore);
            Console.WriteLine("Finished all the games");
        }
        private static void loadFromFileNeuronNetworkAndDemoControl(string path, int sleepTime, int networkIndex = 0) {
            int width = 20, height = 20;
            string line;
            Console.WriteLine("Enter game width or leave blank for " + width);
            line = Console.ReadLine();
            if (!line.Equals(""))
                width = int.Parse(line);
            Console.WriteLine("Enter game height or leave blank for " + height);
            line = Console.ReadLine();
            if (!line.Equals(""))
                height = int.Parse(line);
            Console.CursorTop -= 4;
            Console.WriteLine("                                             ");
            Console.WriteLine("                                             ");
            Console.WriteLine("                                             ");
            Console.WriteLine("                                             ");
            int maxScore = 0;
            EvolutionLab lab = new EvolutionLab();
            NNetwork1 network;
            NNetwork1[] networks;
            if (networkIndex < 0) {
                networks = lab.getGeneration(path, -1, expand: false);
                network = networks[networks.Length + networkIndex];
            }
            else {
                networks = lab.getGeneration(path);
                network = networks[networkIndex];
            }
            Model m = new Model(height, width, sleepTime, Config.INIT_LARGE);
            Config.STEPS_PER_FOOD = height * width;
            IController c = new AutoNNControllerV3(m, network);
            View v = new View(m, 2, 500);
            Console.CursorVisible = false;
            c.play();
            Console.CursorVisible = true;
            maxScore = c.getScore();
        }
        private static void dummySpiralAlgControl() {
            Model m = new Model(40, 20, 30);
            IController c = new AutoSpiralController(m);
            View v = new View(m, 1);
            c.play();
        }
        private static void dummyDirectAlgControl() {
            Model m = new Model(40, 20, 30);
            IController c = new AutoDirectController(m);
            View v = new View(m, 2);

            //Thread viewThread = new Thread(p =>
            //{
            //    ((View)p).play();
            //});
            //viewThread.Start(v);
            //v.displayGame();

            c.play();

            //wait for t3 to finish
            //viewThread.Join();
        }
        private static void manualControl() {
            Model m = new Model(40, 20, 300);
            IController c = new KeyboardController(m);
            View v = new View(m, 2, 200);

            //Thread viewThread = new Thread(p =>
            //{
            //    ((View)p).play();
            //});
            //viewThread.Start(v);
            //v.displayGame();

            c.play();

            //wait for t3 to finish
            //viewThread.Join();
        }
    }
}
