﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace ConsoleApp1 {
    class AutoDirectController : IController {
        private Model model;
        public AutoDirectController(Model _model) {
            model = _model;
            model.AfterMoveEvent += makeNextMove;
        }
        public void reloadModel() { throw new NotImplementedException(); }
        public void setStateVectorMode(StateVectorMode mode) { throw new NotImplementedException(); }
        public int getScore() { return model.getScore(); }
        public void setTurningMode(TurningModes mode) {
            throw new NotImplementedException();
        }
        public NNetwork.Stats getStats() { throw new NotImplementedException(); }
        public void makeNextMove() {
            Point delta = model.head().minus(model.food);
            if (delta.x > 0) {
                switch (model.snakeDirection) {
                    case Directions.UP: break; 
                    case Directions.RIGHT: model.turnLeft(); return;
                    case Directions.DOWN: model.turnLeft(); return;
                    case Directions.LEFT: model.turnRight(); return;
                }
            }
            else if (delta.x < 0) {
                switch (model.snakeDirection) {
                    case Directions.UP: model.turnLeft(); return;
                    case Directions.RIGHT: model.turnRight(); return;
                    case Directions.DOWN: break; 
                    case Directions.LEFT: model.turnLeft(); return;
                }
            }
            else if(delta.y > 0) {
                switch (model.snakeDirection) {
                    case Directions.UP: model.turnLeft(); return;
                    case Directions.RIGHT: model.turnLeft(); return;
                    case Directions.DOWN: model.turnRight(); return;
                    case Directions.LEFT: break;
                }
            }
            else {
                switch (model.snakeDirection) {
                    case Directions.UP: model.turnRight(); return;
                    case Directions.RIGHT: break;
                    case Directions.DOWN: model.turnLeft(); return;
                    case Directions.LEFT: model.turnLeft(); return;
                }
            }
        }

        public void play() {
            Thread modelThread = new Thread(p => {
                ((Model)p).launch();
            });
            modelThread.Start(model);
            modelThread.Join();
        }
    }
}
