﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace ConsoleApp1 {
    using NNetwork;
    class AutoNNControllerV3 : IController {
        public Model model;
        private double maxDistance;
        public int stepsLeft;
        private TurningModes turningMode;
        private StateVectorMode stateVectorMode;
        public static Stats stats;
        private bool initLarge;
        public NNetwork1 nnetwork { get; private set; }
        public void setTurningMode(TurningModes mode) {
            turningMode = mode;
        }
        public void setStateVectorMode(StateVectorMode mode) {
            stateVectorMode = mode;
        }
        Thread modelThread;
        /// <summary>
        /// Food count consumed by a snake in current game
        /// </summary>
        public GameStatus gameStatus { get; private set; }
        public int getScore() { return model.getScore(); }
        public Stats getStats() { return stats; }
        /// <summary>
        /// Initialize controller with randomly generated game board
        /// and specific neural network
        /// </summary>
        public AutoNNControllerV3(NNetwork1 _nnetwork, bool _initLarge) :this(new Model(Config.DEFAULT_HEIGHT, Config.DEFAULT_WIDTH, Config.DEFAULT_SPEED, _initLarge), _nnetwork) {
            initLarge = _initLarge;
        }
        /// <summary>
        /// Initialize controller with specific game board
        /// and specific neural network
        /// </summary>
        public AutoNNControllerV3(Model _model, NNetwork1 _nnetwork) {
            setTurningMode(Config.TURNING_METHOD);
            setStateVectorMode(Config.STATE_VECTOR);
            reloadModel(_model);
            nnetwork = _nnetwork;
            if(stats == null) {
                stats = new Stats(nnetwork.inputCount, nnetwork.outputCount);
            }
        }
        /// <summary>
        /// Generate a new random board for this controller
        /// </summary>
        public void reloadModel() {
            Model _model = new Model(Config.DEFAULT_HEIGHT, Config.DEFAULT_WIDTH, Config.DEFAULT_SPEED, initLarge);
            reloadModel(_model);
        }
        /// <summary>
        /// Change game board for this controller to the specified one
        /// </summary>
        /// <param name="_model"></param>
        public void reloadModel(Model _model) {
            model = _model;
            stepsLeft = Config.STEPS_PER_FOOD;
            maxDistance = Math.Sqrt((model.height - 1) * (model.height - 1) + (model.width - 1) * (model.width - 1));
            model.AfterMoveEvent += makeNextMove;
            model.AfterGrowthEvent += snakeGrew;
        }
        private double getFoodNeuron(Point requestedDirectionVector) {
            return getFoodDistance(requestedDirectionVector, model.head());
        }
        private double getFoodDistance(Point requestedDirectionVector, Point _head) {
            //return _head.plus(requestedDirectionVector).distance(model.food) / maxDistance;
            //double currentFoodDistance = _head.distance(model.food);
            double newFoodDistance = _head.plus(requestedDirectionVector).distance(model.food);
            return newFoodDistance;
            //if(newFoodDistance < currentFoodDistance)
            //    return newFoodDistance / maxDistance;
            //else
            //    return -newFoodDistance / maxDistance;
        }
        private Vector getFoodNeuron() {
            Vector res = new Vector(2);
            Point p = model.head().minus(model.food);
            res.vs[0] = p.x;
            res.vs[1] = p.y;
            res.vs[0] = p.x / maxDistance;
            res.vs[1] = p.y / maxDistance;
            return res;
        }
        private double getWallNeuron(Point requestedDirectionVector) {
            //double currentWallDistance = getWallDistance(model.head());
            double newWallDistance = getWallDistance(requestedDirectionVector, model.head());
            return newWallDistance / maxDistance;
        }
        private double getWallDistance(Point requestedDirectionVector, Point _head) {
            Point pointAhead = _head.plus(requestedDirectionVector);
            int dx = Math.Min(pointAhead.x, model.width), dy = Math.Min(pointAhead.y, model.height);
            double wallDistance = Math.Sqrt((double)dx * dx + dy * dy);
            return wallDistance;
        }
        private double getSnakeNeuron(Point requestedDirectionVector) {
            return getSnakeNeuron(requestedDirectionVector, model.head());
        }
        private double getSnakeNeuron(Point requestedDirectionVector, Point _head) {
            Point pointAhead = _head.plus(requestedDirectionVector);
            while (!model.hasCrashed(pointAhead)) {
                pointAhead = pointAhead.plus(requestedDirectionVector);
            }
            return (pointAhead.distance(_head) - 1) / maxDistance;
            //if (model.isIn(pointAhead)) {
            //    res = pointAhead.distance(_head);
            //}
            //else {
            //    res = maxDistance;
            //}
        }
        private Vector getDirectionStateVectorV1(Point requestedDirectionVector) {
            Vector res = new Vector(3);
            res.vs[0] = getFoodNeuron(requestedDirectionVector);
            res.vs[1] = getWallNeuron(requestedDirectionVector);
            res.vs[2] = getSnakeNeuron(requestedDirectionVector);
            return res;
        }
        private Vector getDirectionStateVectorV2(Point requestedDirectionVector) {
            Vector res = new Vector(3);
            Point pointAhead = model.head().plus(requestedDirectionVector); ;
            while (!model.isWall(pointAhead)) { 
                if (pointAhead.Equals(model.food)){
                    res.vs[0] = 1;
                }
                if (model.isIn(pointAhead)) {
                    //if (model.isIn(pointAhead.x, pointAhead.y)) {
                    res.vs[2] = 1;
                }
                pointAhead = pointAhead.plus(requestedDirectionVector);
            }
            res.vs[1] = 1 / pointAhead.distance(model.head());
            //res.vs[1] = Matrix.sigmoid(pointAhead.distance(model.head()));
            return res;
        }
        private Vector getDirectionStateVectorV4(Point requestedDirectionVector) {
            Vector res = new Vector(3);
            Point pointAhead = model.head().plus(requestedDirectionVector); ;
            while (!model.isWall(pointAhead)) {
                if (pointAhead.Equals(model.food)) {
                    res.vs[0] = 1;
                }
                if (model.isIn(pointAhead)) {
                //if (model.isIn(pointAhead.x, pointAhead.y)) {
                    res.vs[2] = 1;
                }
                pointAhead = pointAhead.plus(requestedDirectionVector);
            }
            //double distance = pointAhead.distance(model.head());
            //res.vs[1] = Matrix.sigmoid(distance);

            //double distance = pointAhead.distance(model.head()) - 1;
            //res.vs[1] = 1 / distance;

            double distance = pointAhead.distance(model.head()) - 1;
            if (distance != 0) {
                res.vs[1] = 1 / distance;
            }
            else {
                res.vs[1] = 1 + 1 / maxDistance;
            }
            return res;
        }
        private Vector getCurentDirection() {
            Vector res = new Vector(4);
            for (int i = 0; i < res.vs.Length; ++i) {
                if(i == (int)model.snakeDirection)
                    res.vs[i] = 1;
                else
                    res.vs[i] = 0;
            }
            return res;
        }
        private Vector getModelStateVectorV1() {
            Vector res = new Vector(nnetwork.inputCount);
            int offset = 0;
            res.merge(getCurentDirection(), offset);
            offset += 4;
            res.merge(getDirectionStateVectorV1(new Point(-1, -1)), offset);
            offset += 3;
            res.merge(getDirectionStateVectorV1(new Point(-1, 0)), offset);
            offset += 3;
            res.merge(getDirectionStateVectorV1(new Point(-1, 1)), offset);
            offset += 3;
            res.merge(getDirectionStateVectorV1(new Point(0, 1)), offset);
            offset += 3;
            res.merge(getDirectionStateVectorV1(new Point(1, 1)), offset);
            offset += 3;
            res.merge(getDirectionStateVectorV1(new Point(1, 0)), offset);
            offset += 3;
            res.merge(getDirectionStateVectorV1(new Point(1, -1)), offset);
            offset += 3;
            res.merge(getDirectionStateVectorV1(new Point(0, -1)), offset);
            return res;
        }
        private Vector getModelStateVectorV2() {
            Vector res = new Vector(nnetwork.inputCount);
            int offset = 0;
            res.merge(getCurentDirection(), offset);
            offset += 4;
            res.merge(getDirectionStateVectorV2(new Point(-1, -1)), offset);
            offset += 3;
            res.merge(getDirectionStateVectorV2(new Point(-1, 0)), offset);
            offset += 3;
            res.merge(getDirectionStateVectorV2(new Point(-1, 1)), offset);
            offset += 3;
            res.merge(getDirectionStateVectorV2(new Point(0, 1)), offset);
            offset += 3;
            res.merge(getDirectionStateVectorV2(new Point(1, 1)), offset);
            offset += 3;
            res.merge(getDirectionStateVectorV2(new Point(1, 0)), offset);
            offset += 3;
            res.merge(getDirectionStateVectorV2(new Point(1, -1)), offset);
            offset += 3;
            res.merge(getDirectionStateVectorV2(new Point(0, -1)), offset);
            return res;
        }
        private Vector getModelStateVectorV3() {
            Vector res = new Vector(nnetwork.inputCount);
            int offset = 0;
            res.merge(getDirectionStateVectorV2(new Point(-1, -1)), offset);
            offset += 3;
            res.merge(getDirectionStateVectorV2(new Point(-1, 0)), offset);
            offset += 3;
            res.merge(getDirectionStateVectorV2(new Point(-1, 1)), offset);
            offset += 3;
            res.merge(getDirectionStateVectorV2(new Point(0, 1)), offset);
            offset += 3;
            res.merge(getDirectionStateVectorV2(new Point(1, 1)), offset);
            offset += 3;
            res.merge(getDirectionStateVectorV2(new Point(1, 0)), offset);
            offset += 3;
            res.merge(getDirectionStateVectorV2(new Point(1, -1)), offset);
            offset += 3;
            res.merge(getDirectionStateVectorV2(new Point(0, -1)), offset);
            return res;
        }
        private Vector getModelStateVectorV4() {
            Vector res = new Vector(nnetwork.inputCount);
            int offset = 0;
            res.merge(getDirectionStateVectorV4(new Point(-1, -1)), offset);
            offset += 3;
            res.merge(getDirectionStateVectorV4(new Point(-1, 0)), offset);
            offset += 3;
            res.merge(getDirectionStateVectorV4(new Point(-1, 1)), offset);
            offset += 3;
            res.merge(getDirectionStateVectorV4(new Point(0, 1)), offset);
            offset += 3;
            res.merge(getDirectionStateVectorV4(new Point(1, 1)), offset);
            offset += 3;
            res.merge(getDirectionStateVectorV4(new Point(1, 0)), offset);
            offset += 3;
            res.merge(getDirectionStateVectorV4(new Point(1, -1)), offset);
            offset += 3;
            res.merge(getDirectionStateVectorV4(new Point(0, -1)), offset);
            return res;
        }
        private Vector getModelStateVector() {
            switch (stateVectorMode) {
                case StateVectorMode.MODE1: return getModelStateVectorV1();
                case StateVectorMode.MODE2: return getModelStateVectorV2();
                case StateVectorMode.MODE3: return getModelStateVectorV3();
                case StateVectorMode.MODE4: return getModelStateVectorV4();
                default: throw new InvalidOperationException("Invalid state vector mode");
            }
        }
        public void makeNextMove() {
            if (stepsLeft-- > 0) {
                Vector modelState = getModelStateVector();
                Vector resultingVector = nnetwork.run(modelState);
                int firedNeuron;
                switch (turningMode) {
                    case TurningModes.ABSOLUTE:
                        Directions dir;
                        int counter = 0;
                        Vector copy = new Vector(resultingVector);
                        //find valid direction with the highest neuron output
                        do {
                            firedNeuron = copy.maxIndex();
                            dir = (Directions)firedNeuron;
                            copy.vs[firedNeuron] = Double.MinValue;
                            counter++;
                        }
                        //check if we need to pick the next highest if this move is invalid
                        while (!model.turn(dir) && counter < resultingVector.vs.Length - 1);
                        break;
                    case TurningModes.RELATIVE:
                        firedNeuron = resultingVector.maxIndex();
                        switch (firedNeuron) {
                            case 0: model.turnLeft(); break;
                            case 1: model.turnRight(); break;
                            default: break;
                        }
                        break;
                    default:
                        throw new InvalidOperationException("Invalid turning mode:" + turningMode);
                }
                stats.resultingNeuronIndex[firedNeuron]++;
                stats.stepsTaken++;
                stats.totalSnakeState.add(modelState);
                stats.totalResultingVector.add(resultingVector);
            }
            else {
                model.gameStatus = GameStatus.OVER;
                stats.ranOutOfStepsCounter++;
            }
        }
        private void snakeGrew(){
            stepsLeft = Config.STEPS_PER_FOOD;
        }
        public void play() {
            modelThread = new Thread(p => {
                ((Model)p).launch();
            });
            modelThread.Start(model);
            modelThread.Join();
            //score = model.getScore();
            if (model.gameStatus == GameStatus.WON) {
                stats.gamesWonCounter++;
                Console.WriteLine("WON A GAME!!!");
            }
        }
    }
}
